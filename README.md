# OpenML dataset: padding-attack-dataset-2023-12-04-cisco-2023-12-04

https://www.openml.org/d/45876

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Bleichenbacher Padding Attack: Dataset created on 2023-12-04 with server 2023-12-04-cisco

Attribute Names:

CCS0:tcp.srcport
CCS0:tcp.dstport
CCS0:tcp.port
CCS0:tcp.stream
CCS0:tcp.len
CCS0:tcp.seq
CCS0:tcp.nxtseq
CCS0:tcp.ack
CCS0:tcp.hdr_len
CCS0:tcp.flags.res
CCS0:tcp.flags.ns
CCS0:tcp.flags.cwr
CCS0:tcp.flags.ecn
CCS0:tcp.flags.urg
CCS0:tcp.flags.ack
CCS0:tcp.flags.push
CCS0:tcp.flags.reset
CCS0:tcp.flags.syn
CCS0:tcp.flags.fin
CCS0:tcp.window_size_value
CCS0:tcp.window_size
CCS0:tcp.window_size_scalefactor
CCS0:tcp.checksum.status
CCS0:tcp.urgent_pointer
CCS0:tcp.options.nop
CCS0:tcp.option_kind
CCS0:tcp.option_len
CCS0:tcp.options.timestamp.tsval
CCS0:tcp.time_delta
CCS0:order
DISC0:tcp.srcport
DISC0:tcp.dstport
DISC0:tcp.port
DISC0:tcp.stream
DISC0:tcp.len
DISC0:tcp.seq
DISC0:tcp.nxtseq
DISC0:tcp.ack
DISC0:tcp.hdr_len
DISC0:tcp.flags.res
DISC0:tcp.flags.ns
DISC0:tcp.flags.cwr
DISC0:tcp.flags.ecn
DISC0:tcp.flags.urg
DISC0:tcp.flags.ack
DISC0:tcp.flags.push
DISC0:tcp.flags.reset
DISC0:tcp.flags.syn
DISC0:tcp.flags.fin
DISC0:tcp.window_size_value
DISC0:tcp.window_size
DISC0:tcp.window_size_scalefactor
DISC0:tcp.checksum.status
DISC0:tcp.urgent_pointer
DISC0:tcp.options.nop
DISC0:tcp.option_kind
DISC0:tcp.option_len
DISC0:tcp.options.timestamp.tsval
DISC0:tcp.time_delta
DISC0:order
DISC1:tcp.srcport
DISC1:tcp.dstport
DISC1:tcp.port
DISC1:tcp.stream
DISC1:tcp.len
DISC1:tcp.seq
DISC1:tcp.nxtseq
DISC1:tcp.ack
DISC1:tcp.hdr_len
DISC1:tcp.flags.res
DISC1:tcp.flags.ns
DISC1:tcp.flags.cwr
DISC1:tcp.flags.ecn
DISC1:tcp.flags.urg
DISC1:tcp.flags.ack
DISC1:tcp.flags.push
DISC1:tcp.flags.reset
DISC1:tcp.flags.syn
DISC1:tcp.flags.fin
DISC1:tcp.window_size_value
DISC1:tcp.window_size
DISC1:tcp.window_size_scalefactor
DISC1:tcp.checksum.status
DISC1:tcp.urgent_pointer
DISC1:tcp.options.nop
DISC1:tcp.option_kind
DISC1:tcp.option_len
DISC1:tcp.options.timestamp.tsval
DISC1:tcp.time_delta
DISC1:order
TLS0:tcp.srcport
TLS0:tcp.dstport
TLS0:tcp.port
TLS0:tcp.stream
TLS0:tcp.len
TLS0:tcp.seq
TLS0:tcp.nxtseq
TLS0:tcp.ack
TLS0:tcp.hdr_len
TLS0:tcp.flags.res
TLS0:tcp.flags.ns
TLS0:tcp.flags.cwr
TLS0:tcp.flags.ecn
TLS0:tcp.flags.urg
TLS0:tcp.flags.ack
TLS0:tcp.flags.push
TLS0:tcp.flags.reset
TLS0:tcp.flags.syn
TLS0:tcp.flags.fin
TLS0:tcp.window_size_value
TLS0:tcp.window_size
TLS0:tcp.window_size_scalefactor
TLS0:tcp.checksum.status
TLS0:tcp.urgent_pointer
TLS0:tcp.options.nop
TLS0:tcp.option_kind
TLS0:tcp.option_len
TLS0:tcp.options.timestamp.tsval
TLS0:tcp.time_delta
TLS0:tls.record.content_type
TLS0:tls.record.length
TLS0:tls.alert_message.level
TLS0:tls.alert_message.desc
TLS0:order
DISC0:tls.record.content_type
DISC0:tls.record.length
DISC0:tls.alert_message.level
DISC0:tls.alert_message.desc
TLS1:tcp.srcport
TLS1:tcp.dstport
TLS1:tcp.port
TLS1:tcp.stream
TLS1:tcp.len
TLS1:tcp.seq
TLS1:tcp.nxtseq
TLS1:tcp.ack
TLS1:tcp.hdr_len
TLS1:tcp.flags.res
TLS1:tcp.flags.ns
TLS1:tcp.flags.cwr
TLS1:tcp.flags.ecn
TLS1:tcp.flags.urg
TLS1:tcp.flags.ack
TLS1:tcp.flags.push
TLS1:tcp.flags.reset
TLS1:tcp.flags.syn
TLS1:tcp.flags.fin
TLS1:tcp.window_size_value
TLS1:tcp.window_size
TLS1:tcp.window_size_scalefactor
TLS1:tcp.checksum.status
TLS1:tcp.urgent_pointer
TLS1:tcp.options.nop
TLS1:tcp.option_kind
TLS1:tcp.option_len
TLS1:tcp.options.timestamp.tsval
TLS1:tcp.time_delta
TLS1:order
DISC2:tcp.srcport
DISC2:tcp.dstport
DISC2:tcp.port
DISC2:tcp.stream
DISC2:tcp.len
DISC2:tcp.seq
DISC2:tcp.nxtseq
DISC2:tcp.ack
DISC2:tcp.hdr_len
DISC2:tcp.flags.res
DISC2:tcp.flags.ns
DISC2:tcp.flags.cwr
DISC2:tcp.flags.ecn
DISC2:tcp.flags.urg
DISC2:tcp.flags.ack
DISC2:tcp.flags.push
DISC2:tcp.flags.reset
DISC2:tcp.flags.syn
DISC2:tcp.flags.fin
DISC2:tcp.window_size_value
DISC2:tcp.window_size
DISC2:tcp.window_size_scalefactor
DISC2:tcp.checksum.status
DISC2:tcp.urgent_pointer
DISC2:tcp.options.nop
DISC2:tcp.option_kind
DISC2:tcp.option_len
DISC2:tcp.options.timestamp.tsval
DISC2:tcp.time_delta
DISC2:order
DISC3:tcp.srcport
DISC3:tcp.dstport
DISC3:tcp.port
DISC3:tcp.stream
DISC3:tcp.len
DISC3:tcp.seq
DISC3:tcp.nxtseq
DISC3:tcp.ack
DISC3:tcp.hdr_len
DISC3:tcp.flags.res
DISC3:tcp.flags.ns
DISC3:tcp.flags.cwr
DISC3:tcp.flags.ecn
DISC3:tcp.flags.urg
DISC3:tcp.flags.ack
DISC3:tcp.flags.push
DISC3:tcp.flags.reset
DISC3:tcp.flags.syn
DISC3:tcp.flags.fin
DISC3:tcp.window_size_value
DISC3:tcp.window_size
DISC3:tcp.window_size_scalefactor
DISC3:tcp.checksum.status
DISC3:tcp.urgent_pointer
DISC3:tcp.options.nop
DISC3:tcp.option_kind
DISC3:tcp.option_len
DISC3:tcp.options.timestamp.tsval
DISC3:tcp.time_delta
DISC3:order
CCS1:tcp.srcport
CCS1:tcp.dstport
CCS1:tcp.port
CCS1:tcp.stream
CCS1:tcp.len
CCS1:tcp.seq
CCS1:tcp.nxtseq
CCS1:tcp.ack
CCS1:tcp.hdr_len
CCS1:tcp.flags.res
CCS1:tcp.flags.ns
CCS1:tcp.flags.cwr
CCS1:tcp.flags.ecn
CCS1:tcp.flags.urg
CCS1:tcp.flags.ack
CCS1:tcp.flags.push
CCS1:tcp.flags.reset
CCS1:tcp.flags.syn
CCS1:tcp.flags.fin
CCS1:tcp.window_size_value
CCS1:tcp.window_size
CCS1:tcp.window_size_scalefactor
CCS1:tcp.checksum.status
CCS1:tcp.urgent_pointer
CCS1:tcp.options.nop
CCS1:tcp.option_kind
CCS1:tcp.option_len
CCS1:tcp.options.timestamp.tsval
CCS1:tcp.time_delta
CCS1:order
DISC1:tls.record.content_type
DISC1:tls.record.length
DISC1:tls.alert_message.level
DISC1:tls.alert_message.desc
vulnerable_classes [0X00 In Pkcs#1 Padding (First 8 Bytes After 0X00 0X02), 0X00 On The Last Position  (|Pms| = 0), Wrong First Byte (0X00 Set To 0X17), Wrong Second Byte (0X02 Set To 0X17)]

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45876) of an [OpenML dataset](https://www.openml.org/d/45876). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45876/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45876/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45876/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

